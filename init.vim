" ~/.config/nvim/init.vim

" Avoid menu.vim (speedup)
let g:did_install_default_menus = 1

" Statusline
set statusline=[%n]\ \ %t\ [%Y/%{&ff}]
set statusline+=%=
set statusline+=col:\ %v,\ line:\ %l/%L\ %p%%\ %m

" Plugins
call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-commentary'                     " Comment stuff out
" Plug 'tpope/vim-surround'                       " Surround with parens etc.
" Plug 'fatih/vim-go', {'for': 'go'}              " Golang support
Plug 'junegunn/vim-easy-align'                  " Align things!
" Plug 'vimwiki/vimwiki'                          " Personal Wiki for notes
" Plug 'justinmk/vim-dirvish'                   " Light weigth directory viewer
Plug 'tpope/vim-eunuch'                         " UNIX commands in vim
" Plug 'raichoo/haskell-vim', {'for': 'haskell'}  " Haskell support
" Plug 'nvie/vim-flake8', {'for': 'python'}       " Flake8 checking for python
" Plug 'elzr/vim-json', {'for': 'json'}           " JSON support
" Plug 'flowtype/vim-flow', {'for': 'javascript'} " Flow intergration for js
" Plug 'neomake/neomake'                          " Async make and linting
Plug 'reasonml/vim-reason-loader'               " Reasonml support
call plug#end()

filetype plugin indent on

" set autochdir  " Change working directory to current buffer's
set number       " Line numbers
set tabstop=4    " Tabs are 8 spaces
set shiftwidth=4 " Shifting with >> and << is 8 spaces
set expandtab    " Expand tabs to spaces
     " set modeline   " Show modeline at bottom of window
set smartcase    " If upcase is used in search then ignore 'ignorecase'
set ignorecase   " Ignore case in search
set showmatch    " Highlight regexp matches
set lazyredraw   " Don't redraw so much
set textwidth=80 " Break text after 80 chars
set inccommand=nosplit
set cc=80        " Highlight 80th column red
hi ColorColumn ctermbg=lightred
set noswapfile   " Swap files are annoying
syntax on        " Syntax highlighting
set cursorline   " Highlight the current line
hi cursorline cterm=None ctermbg=white ctermfg=black

" Setup quickfix
set shellpipe=2>
set errorfile=error.txt

" Ocaml setup
set rtp^="/Users/nickspain/.opam/system/share/ocp-indent/vim"
let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"

" Remove whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" C file settings
augroup c_lang
    au!
    autocmd BufwritePost *.c,*.h :silent !ctags -R .
    autocmd BufRead *.c,*.h :silent !ctags -R .
    autocmd BufRead *c,*.h :set tags+=tags
augroup END

" Python file settings
augroup py_lang
    au!
    command! Pep8 :call Flake8()<CR>
augroup END

" TeX file settings
augroup tex_latex
        au!
        autocmd Bufread *.tex,*.ltx :setlocal cc=90
        autocmd Bufread *.tex,*.ltx :setlocal textwidth=80
        autocmd Bufread *.tex,*.ltx :setlocal tabstop=2
        autocmd Bufread *.tex,*.ltx :setlocal shiftwidth=2
augroup END


" HTML file settings
augroup html
        au!
        autocmd Bufread *.html :setlocal tabstop=2
        autocmd Bufread *.html :setlocal shiftwidth=2
augroup END


" Erlang file settings
augroup erlang
        au!
        autocmd Bufread *.erl :setlocal tabstop=2
        autocmd Bufread *.erl :setlocal shiftwidth=2
augroup END

" My Commands
command! Init :e $MYVIMRC
command! TodoInit :e $HOME/.config/nvim/TODO.txt
command! PyTemplate :r !cat $HOME/.config/nvim/templates/py.template
command! CTemplate :r !cat $HOME/.config/nvim/templates/c.template
command! HTemplate :r !cat $HOME/.config/nvim/templates/h.template
command! CPPTemplate :r !cat $HOME/.config/nvim/templates/cpp.template
command! Flasktemplate :r !cat $HOME/.config/nvim/templates/flask.template
command! TexTemplate :r !cat $HOME/.config/nvim/templates/tex.template
command! XMLTemplate :r !cat $HOME/.config/nvim/templates/xml.template
command! HTMLTemplate :r !cat $HOME/.config/nvim/templates/html.template

